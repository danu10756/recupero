import java.util.*;
public class Book{
	
	public static final String RED = "\033[0;31m";
	public static final String RESET = "\033[0m";
	public static final String WHITE = "\033[0;37m";
	public static final String PURPLE = "\033[0;35m";
	static Random rnd=new Random();
	static int nLibri=0;
	String titolo;
	String autore;
	int pagine;
	static Book[] biblioteca=new Book[20];
	

	public Book(String titolo, String autore, int pagine){
		this.titolo=titolo;
		this.autore=autore;
		this.pagine=pagine;
		nLibri++;
	}

	public Book(){
		
	}

	public static void bibliotecaSwap(Book ia, Book ib ){   
		String keepTitle="";
		String keepAutor="";
		int keepPage=0;
		
		keepTitle=ia.titolo;
		keepAutor=ia.autore;
		keepPage=ia.pagine;

		ia.titolo=ib.titolo;
		ia.autore=ib.autore;
		ia.pagine=ib.pagine;

		ib.titolo=keepTitle;
		ib.autore=keepAutor;
		ib.pagine=keepPage;

		}	

	public static Book[] titleSort(Book[] biblioteca){
		for(int i=0;i<biblioteca.length;i++){
			for(int j=i+1;j<biblioteca.length;j++){
				if(biblioteca[i].titolo.compareTo(biblioteca[j].titolo)<0){
					bibliotecaSwap(biblioteca[i], biblioteca[j]);
				}
			}
		}
		return biblioteca;
	} 

	public static Book[] autoreSort(Book[] a){
		for(int i=0;i<biblioteca.length;i++){
			for(int j=i+1;j<biblioteca.length;j++){
				if(biblioteca[i].autore.compareTo(biblioteca[j].autore)<0){
					bibliotecaSwap(a[i], a[j]);
				}
			}
		}
		return biblioteca;
	}

	

	public static Book factory(){
		Book libro =new Book();
		int pag=0;
		String title="";
		String write="";
		for(int i=0;i<10;i++){  //creo titolo
			title += (char)('a'+rnd.nextInt(26));
		}
		for(int i=0; i<8; i++){ //creo autore
			write += (char)('a'+rnd.nextInt(26));
		}
		pag=rnd.nextInt(20)+80;
		libro.titolo=title;
		libro.autore=write;
		libro.pagine=pag;
		nLibri++;
		return libro;

	}

	public static void main(String[] args){
		biblioteca=titleSort(biblioteca);
		biblioteca=autoreSort(biblioteca);
		for(int i=0;i<biblioteca.length;i++){
			biblioteca[i]=factory();
			System.out.print("Titolo:  "+RED+biblioteca[i].autore);
			System.out.print(RESET+"     Autore:  "+WHITE+biblioteca[i].autore);
			System.out.print(RESET+"    Pagine:  "+PURPLE+biblioteca[i].pagine+"\n"+RESET);
			

		}
		System.out.println();
	}

}